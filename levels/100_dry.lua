name = "Stranded"
map = [[

      
    g  
   ggg     gg            a  a
wwgggggp  bhg           ggggggggg
ggggggggggggggwwwwwwwgggggggggggg
ggggggggggggggggggggggggggggggggg
]]

intro = "Got nothing to do? Press Enter to wait a step."

level = Level:new(name, map, intro)

return level
